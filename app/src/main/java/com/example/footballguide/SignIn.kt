package com.example.footballguide

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*


class SignIn : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signInButton.setOnClickListener {
            signIn()
        }
    }

    private fun signIn() {
        val email = emailET.text.toString()
        val password = passwordET.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()) {
            progressBar.visibility = View.VISIBLE
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    progressBar.visibility = View.GONE
                    if (task.isSuccessful) {
                        d("signIn", "signInWithEmail:success")
                        val intent = Intent(this, FootballInformationChooser::class.java)
                        startActivity(intent)
                    } else {
                        d("signIn", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "${task.exception}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        } else {
            Toast.makeText(this, "Please fill the fields!", Toast.LENGTH_SHORT).show()
        }
    }
}
