package com.example.footballguide

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_hard_questions_answers.*

class HardQuestionsAnswers : AppCompatActivity() {
    private var myScore = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hard_questions_answers)
        init()
    }

    private fun init() {
        hardAnswer1()
        hardAnswer2()
        hardAnswer3()
        hardAnswer4()
        hardAnswer5()
    }

    private fun hardAnswer1() {
        val hardAnswer1 = intent.extras?.getString("hardQuestion1", "")?.toLowerCase()
        hardAnswer1TV.text = hardAnswer1
        if (hardAnswer1 == "cannes") {
            correctness1TV.text = "Correct"
            correctness1TV.setBackgroundColor(Color.GREEN)
            myScore++
            hardScoreTV.text = "My score is: $myScore"
        } else {
            myScore += 0
            hardScoreTV.text = "My score is: $myScore"
            correctness1TV.text = "Incorrect"
            correctness1TV.setBackgroundColor(Color.RED)
        }
    }

    private fun hardAnswer2() {
        val hardAnswer2 = intent.extras?.getString("hardQuestion2", "")?.toLowerCase()
        hardAnswer2TV.text = hardAnswer2
        if (hardAnswer2 == "hand of god" || hardAnswer2 == "god's hand" || hardAnswer2 == "hand of a god" || hardAnswer2 == "hand of the god") {
            correctness2TV.text = "Correct"
            correctness2TV.setBackgroundColor(Color.GREEN)
            myScore++
            hardScoreTV.text = "My score is: $myScore"
        } else {
            myScore += 0
            hardScoreTV.text = "My score is: $myScore"
            correctness2TV.text = "Incorrect"
            correctness2TV.setBackgroundColor(Color.RED)
        }
    }

    private fun hardAnswer3() {
        val hardAnswer3 = intent.extras?.getString("hardQuestion3", "")?.toLowerCase()
        hardAnswer3TV.text = hardAnswer3
        if (hardAnswer3 == "inter" || hardAnswer3 == "inter milan") {
            correctness3TV.text = "Correct"
            correctness3TV.setBackgroundColor(Color.GREEN)
            myScore++
            hardScoreTV.text = "My score is: $myScore"
        } else {
            myScore += 0
            hardScoreTV.text = "My score is: $myScore"
            correctness3TV.text = "Incorrect"
            correctness3TV.setBackgroundColor(Color.RED)
        }
    }

    private fun hardAnswer4() {
        val hardAnswer4 = intent.extras?.getString("hardQuestion4", "")
        hardAnswer4TV.text = hardAnswer4
        if (hardAnswer4 == "34") {
            correctness4TV.text = "Correct"
            correctness4TV.setBackgroundColor(Color.GREEN)
            myScore++
            hardScoreTV.text = "My score is: $myScore"
        } else {
            myScore += 0
            hardScoreTV.text = "My score is: $myScore"
            correctness4TV.text = "Incorrect"
            correctness4TV.setBackgroundColor(Color.RED)
        }
    }

    private fun hardAnswer5() {
        val hardAnswer5 = intent.extras?.getString("hardQuestion5", "")?.toLowerCase()
        hardAnswer5TV.text = hardAnswer5
        if (hardAnswer5 == "gremio") {
            correctness5TV.text = "Correct"
            correctness5TV.setBackgroundColor(Color.GREEN)
            myScore++
            hardScoreTV.text = "My score is: $myScore"
        } else {
            myScore += 0
            hardScoreTV.text = "My score is: $myScore"
            correctness5TV.text = "Incorrect"
            correctness5TV.setBackgroundColor(Color.RED)
        }
    }


    fun mainPage(view: View) {
        val intent = Intent(this, FootballInformationChooser::class.java)
        startActivity(intent)
        myScore = 0
    }
}