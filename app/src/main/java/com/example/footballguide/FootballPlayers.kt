package com.example.footballguide

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_football_clubs.*
import kotlinx.android.synthetic.main.activity_football_clubs.imageView1
import kotlinx.android.synthetic.main.activity_football_clubs.imageView2
import kotlinx.android.synthetic.main.activity_football_clubs.imageView3
import kotlinx.android.synthetic.main.activity_football_clubs.imageView4
import kotlinx.android.synthetic.main.activity_football_clubs.imageView5
import kotlinx.android.synthetic.main.activity_football_information_chooser.*
import kotlinx.android.synthetic.main.activity_football_players.*
import kotlinx.android.synthetic.main.activity_football_players.backButton

class FootballPlayers : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_football_players)
        init()
    }

    private fun init() {
        Glide.with(this)
            .load("https://welsh-premier.com/wp-content/uploads/2020/01/Ronaldo-nazario.jpg")
            .into(imageView1);
        Glide.with(this)
            .load("https://i2-prod.mirror.co.uk/incoming/article7633922.ece/ALTERNATES/s615b/Dutch-football-legend-Johan-Cruyff.jpg")
            .into(imageView2);
        Glide.with(this)
            .load("https://www.thenation.com/wp-content/uploads/2020/11/diego-maradona-gt-img.jpg")
            .into(imageView3);
        Glide.with(this)
            .load("https://sl.sbs.com.au/public/image/file/577b679f-2328-4985-965a-c0dc3c500461/crop/1x1")
            .into(imageView4);
        Glide.with(this)
            .load("https://cdn.mos.cms.futurecdn.net/2hTFcHEgbvhQNhjSt945tC-1200-80.jpg")
            .into(imageView5);
        setClick()
    }

    private fun setClick() {
        imageView1.setOnClickListener {
            ronaldoNazario()
        }
        imageView2.setOnClickListener {
            johanCruyff()
        }
        imageView3.setOnClickListener {
            diegoMaradona()
        }
        imageView4.setOnClickListener {
            ronaldinho()
        }
        imageView5.setOnClickListener {
            zinedineZidane()
        }
        backButton.setOnClickListener {
            backButton.visibility = View.GONE
            playerInfoTV.visibility = View.GONE
            playersScrollView.visibility = View.VISIBLE
        }
    }

    private fun ronaldoNazario() {
        playersScrollView.visibility = View.GONE
        playerInfoTV.visibility = View.VISIBLE
        backButton.visibility = View.VISIBLE
        playerInfoTV.text =
            "Ronaldo luis nazario was born on 18 september, 1976 in brazil. his first football" +
                    " club was Cruzeiro. then his career continued in psv. because of his game, he joined barcelona for 1 year. in 1997 he was" +
                    " playing for inter in itally and people called him Fenomeno. in inter he collected many personal" +
                    " award: golden ball, golden boot, Fifa world player of the year, Seria A footballer of the year.Ronaldo has won 3 Fifa world " +
                    "cup and this tournament he is the all time top scorer with 15 goals in 19 games.he also played for " +
                    "real madrid and milan. after the career he won many as individual as teamwork awards, which included 2" +
                    " golden ball. he scored 247 goals in 343 games."
    }

    private fun johanCruyff() {
        playersScrollView.visibility = View.GONE
        playerInfoTV.visibility = View.VISIBLE
        backButton.visibility = View.VISIBLE
        playerInfoTV.text =
            "Johan Cruyff was born on 25 april, 1947 in Netherlands.he started his career at Ajax where he won " +
                    "eight Eredivision titles, three European Cups and one international cup. in 1973 he moved to Barcelona for" +
                    " a world record transfer fee. barcelona's stadium Camp Now is named after Johan Cruyff. he was successful as player" +
                    " as manager. he has won 3 golden ball. he has scored  291 goals in 520 games in professional career" +
                    " and 33 goals in 48 games for Netherlands"
    }

    private fun diegoMaradona() {
        playersScrollView.visibility = View.GONE
        playerInfoTV.visibility = View.VISIBLE
        backButton.visibility = View.VISIBLE
        playerInfoTV.text =
            "Diego Maradona was born on 30 october, 1960 in Argentina. he has won best player of the century with Pele." +
                    " Maradona was the first player to set the world record transfer fee twice, when he moved to Barcelona and when he moved" +
                    " to Napoli. he has won 1986 World cup and won the Golden Ball as the tournament's best player. in the quarter final his team" +
                    " victory over England and both goals scored.\n first goal scored by hand and it's known as the Hand of God." +
                    " he has scored 259 goals in 491 games as professional career and 34 goals in 91 games for Argentina."
    }

    private fun ronaldinho() {
        playersScrollView.visibility = View.GONE
        playerInfoTV.visibility = View.VISIBLE
        backButton.visibility = View.VISIBLE
        playerInfoTV.text =
            "Ronaldinho was born on 21 march, 1980 in Brazil. he made his career debut for Gremio in 1998.at age 20" +
                    " he moved to PSG, and in 2003 he joiner in Barcelona. in his second season with Barcelona, he won his first Fifa World" +
                    " Player of the year award. in 2005 he won golden ball and his second Fifa World player of the year.Ronaldinho became the" +
                    " second Barca player after Diego Maradona, to receive a standing ovation from Real Madrid fans at Santiago Bernabeu. he" +
                    " won world cup in 2002. in 2009 ronaldinho won golden foot. he has scored 167 goals in 441 games as professional career and" +
                    " 33 goals in 97 games for Brazil."
    }

    private fun zinedineZidane() {
        playersScrollView.visibility = View.GONE
        playerInfoTV.visibility = View.VISIBLE
        backButton.visibility = View.VISIBLE
        playerInfoTV.text =
            "Zinedine Zidane was born on 23 june,1972 in French.he started his career at Cannes. in 1996 he moved to Juventus," +
                    " where he won two Serie A titles. in 2001 he moved to Real Madrid for a world record fee at the time.Zidane won the 1998 Fifa" +
                    " world cup and Uedo Euro 2000. he received many individual awards, which inlcluding Fifa World Player of the year at three" +
                    " times and one golden ball.he scored 95 goals in 506 games as professional career and 31 goals for France. Zidane is the only" +
                    " one manager who has Uefa Chempions League three times in a row."
    }
    fun quiz(View:View){
        startActivity(Intent(this,QuizDifficultyChooser::class.java))
    }
    fun mainPage(View: View){
        startActivity(Intent(this,Authentication::class.java))
    }
}