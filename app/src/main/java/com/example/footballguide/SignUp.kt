package com.example.footballguide

import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUp : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signUpButton.setOnClickListener {
            signUp()
        }
    }

    private fun signUp() {
        val email = emailET.text.toString()
        val password = passwordET.text.toString()
        val repeatPassword = repeatPassET.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()) {
            if (password == repeatPassword) {
                signUpButton.isClickable = false
                progressBar.visibility = View.VISIBLE
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        signUpButton.isClickable = true
                        progressBar.visibility = View.GONE
                        if (task.isSuccessful) {
                            d("signUp", "createUserWithEmail:success")
                            val intent = Intent(this, FootballInformationChooser::class.java)
                            startActivity(intent)
                        } else {
                            d("signUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                this, "${task.exception}",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(this, "Passwords do not match!", Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(this, "Please fill the fields!", Toast.LENGTH_SHORT).show()
        }
    }
}
