package com.example.footballguide

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_easy_questions_answer.*
import kotlinx.android.synthetic.main.activity_football_clubs.*

class EasyQuestionsAnswer : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_easy_questions_answer)
        init()
    }
    private fun init(){
        easyScoreTV.text = "My Score is $score"
        scoreImage()

    }
     private fun scoreImage(){
        if (score==0){
            Glide.with(this)
                .load("https://toptests.co.uk/wp-content/uploads/2017/02/failed-theory-test.png")
                .into(scoreImage);
        }else if(score>0 && score < 3 ){
            Glide.with(this)
                .load("https://i1.kym-cdn.com/photos/images/facebook/000/258/194/bc9.png")
                .into(scoreImage);
        }else if (score == 3 || score == 4){
            Glide.with(this)
                .load("https://blog.brookespublishing.com/wp-content/uploads/2016/11/good-job.jpeg")
                .into(scoreImage);
        }else if (score == 5){
            Glide.with(this)
                .load("https://skimpyliving.files.wordpress.com/2020/05/skimpy-amazing-results.png")
                .into(scoreImage);
        }
    }
    fun homePage(view: View){
        startActivity(Intent(this,FootballInformationChooser::class.java))
        score = 0
    }
}