package com.example.footballguide

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_football_clubs.*

class FootballClubs : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_football_clubs)
        init()
    }

    private fun init() {
        Glide.with(this)
            .load("https://www.pixelstalk.net/wp-content/uploads/2016/05/FC-Barcelona-Logo-iPhone-Wallpaper.jpg")
            .into(imageView1);
        Glide.with(this)
            .load("https://totalfootballanalysis.com/wp-content/uploads/2019/08/realmadrid.jpg")
            .into(imageView2);
        Glide.with(this)
            .load("https://www.logolynx.com/images/logolynx/fa/faf5c591b4cd31e44510cf8074c2c32a.jpeg")
            .into(imageView3);
        Glide.with(this)
            .load("https://www.101greatgoals.com/wp-content/uploads/2017/03/422770-fc-liverpool-logo.jpg")
            .into(imageView4);
        Glide.with(this)
            .load("https://inspirationseek.com/wp-content/uploads/2014/05/Inter-Milan-Logo-Wallpaper-1024x576.jpg")
            .into(imageView5);
        setClick()
    }

    private fun setClick() {
        imageView1.setOnClickListener {
            barcelona()
        }
        imageView2.setOnClickListener {
            realMadrid()
        }
        imageView3.setOnClickListener {
            milan()
        }
        imageView4.setOnClickListener {
            liverpool()
        }
        imageView5.setOnClickListener {
            inter()
        }
        backButton.setOnClickListener {
            backButton.visibility = View.GONE
            informationTV.visibility = View.GONE
            scrollView.visibility = View.VISIBLE
        }
    }

    private fun barcelona() {
        scrollView.visibility = View.GONE
        backButton.visibility = View.VISIBLE
        informationTV.visibility = View.VISIBLE
        informationTV.text = "Football Club Barcelona is a Spanish professional football club based " +
                "in Barcelona, Founded in 1899 by a group of Swiss, Spanish, English, and Catalan " +
                "footballers led by Joan GamPer, the club has become a symbol of Catalan culture and" +
                " CaTaLanIsm, hence the motto \"Més que un club\" (\"More than a club\"). Barcelona" +
                " has won a 94 titles, that includes 5 UeFA ChemPiOns league. BaRca\'s stadium is" +
                " Camp Nou, that is the largest stadium in europe, with a seating capacity of 99,354." +
                " all time scorer is lionel MeSsi, with 650 goals and still playing. he has won 6 FIFa BaLLon d\'OR and 6 golden boots."
    }
    private fun realMadrid() {
        scrollView.visibility = View.GONE
        informationTV.visibility = View.VISIBLE
        backButton.visibility = View.VISIBLE
        informationTV.text = "Real Madrid, is a Spanish professional football club based in Madrid. Founded" +
                " on 6 March 1902 as Madrid Football Club. The word real is Spanish for \"royal\" and was" +
                " bestowed to the club by King Alfonso XIII in 1920 together with the royal crown in the" +
                " emblem. club\'s stadium is Santiago BernAbeu, capacity  is 81,044.club has won 92 titles," +
                " that includes 13 UeFA ChemPiOns league. Real madrid is the most titular football team. all-time" +
                "  top scorer is Cristiano RonaLdo, with 450 goals,he has 5 FiFA BalLon d\'OR and 4 golden boots."
    }

    private fun milan() {
        scrollView.visibility = View.GONE
        informationTV.visibility = View.VISIBLE
        backButton.visibility = View.VISIBLE
        informationTV.text = "Milan, is a professional football club in Milan, Italy. A.C. Milan was founded as Milan " +
                "FootBall and Cricket Club in 1899 by English expatriates Alfred Edwards and Herbert KilPin." +
                "Milan is the most titular team in italy, it has won 50 titles,that includes 3 UeFA ChemPiOns league and" +
                "4 UeFA Europe cup. club\'s stadium is San SiRo, capacity is 80,018. All time top scorer is  GunNar NorAhl," +
                " with 221 goals"
    }
    private fun liverpool(){
        scrollView.visibility = View.GONE
        backButton.visibility = View.VISIBLE
        informationTV.visibility = View.VISIBLE
        informationTV.text = "Liverpool Football Club is a professional football club in Liverpool, England.Liverpool was founded by " +
                "John HouLDing in june 3, 1892. Liverpool has won 69 titles,that includes 2 UeFA ChemPIons league and" +
                "4 UeFA Europe cup club\'s stadium is AnField, capacity is 53,394. all time top scorer is Ian Rush with 346" +
                " goals. he has won 1 golden boot "
    }

    private fun inter(){
        scrollView.visibility = View.GONE
        informationTV.visibility = View.VISIBLE
        backButton.visibility = View.VISIBLE
        informationTV.text = "Football Club InterNaZioNaLe MiLaNo is an Italian professional football club based in Milan, Lombardy." +
                " Inter is the only Italian club which never relegated from the top championship of Italian football." +
                "Founded in 9 march, 1908 following a schism within the Milan Cricket and Football Club. Inter has won 39 titles" +
                "that includes 1 UeFA ChemPIons league and 2 UeFA Europe cup.club\'s stadium is San SiRo, capacity is 80,018." +
                "all time scorer is Giuseppe MeaZza with 284 goals. he has won FiFa world cup golden ball"
    }
    fun quiz(view: View){
        val intent = Intent(this,QuizDifficultyChooser::class.java)
        startActivity(intent)
    }
    fun mainPage(view: View){
        val intent = Intent(this,Authentication::class.java)
        startActivity(intent)
    }
}
