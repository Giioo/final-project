package com.example.footballguide

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_hard_questions.*


class HardQuestions : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hard_questions)
        init()
    }

    private fun init() {
        checkScore.setOnClickListener {
            if (hardQuestion1.text.toString().isNotEmpty() && hardQuestion2.text.toString()
                    .isNotEmpty() && hardQuestion3.text.toString()
                    .isNotEmpty() && hardQuestion4.text.toString()
                    .isNotEmpty() && hardQuestion5.text.toString().isNotEmpty()
            ) {
                val intent = Intent(this, HardQuestionsAnswers::class.java)
                intent.putExtra("hardQuestion1", hardQuestion1.text.toString())
                intent.putExtra("hardQuestion2", hardQuestion2.text.toString())
                intent.putExtra("hardQuestion3", hardQuestion3.text.toString())
                intent.putExtra("hardQuestion4", hardQuestion4.text.toString())
                intent.putExtra("hardQuestion5", hardQuestion5.text.toString())
                startActivity(intent)
            } else {
                Toast.makeText(this, "Please fill all question!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun homePage(view: View) {
        val intent = Intent(this, FootballClubs::class.java)
        startActivity(intent)
    }
}