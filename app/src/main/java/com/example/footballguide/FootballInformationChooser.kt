package com.example.footballguide

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_football_information_chooser.*

class FootballInformationChooser : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_football_information_chooser)
        init()
    }

    private fun init() {
        clubsButton.setOnClickListener {
            startActivity(Intent(this,FootballClubs::class.java))
        }
        playersButton.setOnClickListener {
            startActivity(Intent(this,FootballPlayers::class.java))
        }
        mainPageButton.setOnClickListener {
            startActivity(Intent(this,Authentication::class.java))
        }
    }
}