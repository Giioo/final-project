package com.example.footballguide

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_quiz_difficulty_chooser.*

class QuizDifficultyChooser : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_difficulty_chooser)
        init()
    }
    private fun init(){
        easyButton.setOnClickListener {
            startActivity(Intent(this,EasyQuestions::class.java))
        }
        mediumButton.setOnClickListener {
            startActivity(Intent(this,MediumQuestions::class.java))
        }
        hardButton.setOnClickListener {
            startActivity(Intent(this,HardQuestions::class.java))
        }
    }
}