package com.example.footballguide

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_medium_questions.*

class MediumQuestions : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_medium_questions)
        init()
    }

    private fun init() {
        mediumQuestion3()
        mediumQuestion4()
        mediumQuestion5()
    }

    private fun mediumQuestion3() {
        wrongAnswer1TV.setOnClickListener {
            wrongAnswer1TV.setBackgroundColor(Color.RED)
            score += 0
            wrongAnswer1TV.isClickable = false
            wrongAnswer2TV.isClickable = false
            correctAnswer1TV.isClickable = false
        }
        wrongAnswer2TV.setOnClickListener {
            wrongAnswer2TV.setBackgroundColor(Color.RED)
            score += 0
            wrongAnswer1TV.isClickable = false
            wrongAnswer2TV.isClickable = false
            correctAnswer1TV.isClickable = false
        }
        correctAnswer1TV.setOnClickListener {
            correctAnswer1TV.setBackgroundColor(Color.GREEN)
            score++
            wrongAnswer1TV.isClickable = false
            wrongAnswer2TV.isClickable = false
            correctAnswer1TV.isClickable = false
        }
    }

    private fun mediumQuestion4() {
        wrongMotto1TV.setOnClickListener {
            wrongMotto1TV.setBackgroundColor(Color.RED)
            score += 0
            wrongMotto1TV.isClickable = false
            wrongMotto2TV.isClickable = false
            correctMottoTV.isClickable = false
        }
        wrongMotto2TV.setOnClickListener {
            wrongMotto2TV.setBackgroundColor(Color.RED)
            score += 0
            wrongMotto1TV.isClickable = false
            wrongMotto2TV.isClickable = false
            correctMottoTV.isClickable = false
        }
        correctMottoTV.setOnClickListener {
            correctMottoTV.setBackgroundColor(Color.GREEN)
            score++
            wrongMotto1TV.isClickable = false
            wrongMotto2TV.isClickable = false
            correctMottoTV.isClickable = false
        }
    }

    private fun mediumQuestion5() {
        campNouTV.setOnClickListener {
            campNouTV.setBackgroundColor(Color.GREEN)
            score++
            campNouTV.isClickable = false
            santiagoBernabueTV.isClickable = false
            anfieldTV.isClickable = false
        }
        santiagoBernabueTV.setOnClickListener {
            santiagoBernabueTV.setBackgroundColor(Color.RED)
            score += 0
            campNouTV.isClickable = false
            santiagoBernabueTV.isClickable = false
            anfieldTV.isClickable = false
        }
        anfieldTV.setOnClickListener {
            anfieldTV.setBackgroundColor(Color.RED)
            score += 0
            campNouTV.isClickable = false
            santiagoBernabueTV.isClickable = false
            anfieldTV.isClickable = false
        }
    }

    fun submitButton(view: View) {
        if (inputAnswer1ET.text.toString().isNotEmpty() && inputAnswer2ET.text.toString().isNotEmpty()){

            if (!wrongAnswer1TV.isClickable || !wrongAnswer2TV.isClickable || !correctAnswer1TV.isClickable){

                if (!wrongMotto1TV.isClickable || !wrongMotto2TV.isClickable || !correctMottoTV.isClickable){

                    if (!campNouTV.isClickable || !santiagoBernabueTV.isClickable || !anfieldTV.isClickable){

                        val intent = Intent(this, MediumQuestionsAnswers::class.java)
                        intent.putExtra("mediumQuestion1", inputAnswer1ET.text.toString())
                        intent.putExtra("mediumQuestion2",inputAnswer2ET.text.toString())
                        startActivity(intent)
                    }else{
                        Toast.makeText(this, "Please fix the questions!", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    Toast.makeText(this, "please fix the questions!", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this, "please fix the questions!", Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this, "please fix the questions!", Toast.LENGTH_SHORT).show()
        }
    }
}