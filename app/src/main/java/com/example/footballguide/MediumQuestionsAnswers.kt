package com.example.footballguide

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_easy_questions_answer.*
import kotlinx.android.synthetic.main.activity_easy_questions_answer.easyScoreTV
import kotlinx.android.synthetic.main.activity_medium_questions.*
import kotlinx.android.synthetic.main.activity_medium_questions_answers.*

class MediumQuestionsAnswers : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_medium_questions_answers)
        init()
    }
    private fun init(){
        mediumAnswer1()
        mediumAnswer2()
    }
    private fun mediumAnswer1(){
        val mediumAnswer1 = intent.extras?.getString("mediumQuestion1","")!!.toLowerCase()
        mediumAnswer1TV.text = mediumAnswer1
        if (mediumAnswer1 == "fenomeno"){
            score++
            mediumScoreTV.text = "My Score is $score"
            mediumAnswer1TV.setBackgroundColor(Color.GREEN)
        }else{
            score+=0
            mediumScoreTV.text = "My Score is $score"
            mediumAnswer1TV.setBackgroundColor(Color.RED)
        }
    }
    private fun mediumAnswer2(){
        val mediumAnswer2 = intent.extras?.getString("mediumQuestion2","")!!.toLowerCase()
        mediumAnswer2TV.text = mediumAnswer2
        if (mediumAnswer2 == "johan" || mediumAnswer2 == "cruyff" || mediumAnswer2 == "johan cruyff" ){
            score++
            mediumScoreTV.text = "My Score is $score"
            mediumAnswer2TV.setBackgroundColor(Color.GREEN)
        }else{
            score+=0
            mediumScoreTV.text = "My Score is $score"
            mediumAnswer2TV.setBackgroundColor(Color.RED)
        }
    }
    fun homePage(view: View){
        startActivity(Intent(this,FootballInformationChooser::class.java))
        score = 0
    }
}