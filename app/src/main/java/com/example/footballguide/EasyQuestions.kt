package com.example.footballguide

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_easy_questions.*


class EasyQuestions : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_easy_questions)
        init()
    }

    private fun init() {
        easyQuestion1()
        easyQuestion2()
        easyQuestion3()
        easyQuestion4()
        easyQuestion5()
    }

    private fun easyQuestion1() {
        messiTV.setOnClickListener {
            messiTV.setBackgroundColor(Color.GREEN)
            score++
            ronaldoTV.isClickable = false
            ronaldinhoTV.isClickable = false
            messiTV.isClickable = false
        }
        ronaldoTV.setOnClickListener {
            ronaldoTV.setBackgroundColor(Color.RED)
            messiTV.isClickable = false
            score += 0
            ronaldoTV.isClickable = false
            ronaldinhoTV.isClickable = false
        }
        ronaldinhoTV.setOnClickListener {
            ronaldinhoTV.setBackgroundColor(Color.RED)
            messiTV.isClickable = false
            score += 0
            ronaldinhoTV.isClickable = false
            ronaldoTV.isClickable = false
        }
    }

    private fun easyQuestion2() {
        realMadridTV.setOnClickListener {
            realMadridTV.setBackgroundColor(Color.RED)
            realMadridTV.isClickable = false
            milanTV.isClickable = false
            score += 0
            liverpoolTV.isClickable = false
        }
        milanTV.setOnClickListener {
            milanTV.setBackgroundColor(Color.RED)
            milanTV.isClickable = false
            liverpoolTV.isClickable = false
            score += 0
            realMadridTV.isClickable = false
        }
        liverpoolTV.setOnClickListener {
            liverpoolTV.setBackgroundColor(Color.GREEN)
            milanTV.isClickable = false
            score++
            realMadridTV.isClickable = false
            liverpoolTV.isClickable = false
        }
    }

    private fun easyQuestion3() {
        cruyffTV.setOnClickListener {
            cruyffTV.setBackgroundColor(Color.RED)
            score += 0
            cruyffTV.isClickable = false
            guardiolaTV.isClickable = false
            zidaneTV.isClickable = false
        }
        guardiolaTV.setOnClickListener {
            guardiolaTV.setBackgroundColor(Color.RED)
            score += 0
            cruyffTV.isClickable = false
            guardiolaTV.isClickable = false
            zidaneTV.isClickable = false
        }
        zidaneTV.setOnClickListener {
            zidaneTV.setBackgroundColor(Color.GREEN)
            score++
            cruyffTV.isClickable = false
            guardiolaTV.isClickable = false
            zidaneTV.isClickable = false
        }
    }

    private fun easyQuestion4() {
        ball4TV.setOnClickListener {
            ball4TV.setBackgroundColor(Color.RED)
            score += 0
            ball4TV.isClickable = false
            ball5TV.isClickable = false
            ball6TV.isClickable = false
        }
        ball5TV.setOnClickListener {
            ball5TV.setBackgroundColor(Color.GREEN)
            score++
            ball5TV.isClickable = false
            ball6TV.isClickable = false
            ball4TV.isClickable = false
        }
        ball6TV.setOnClickListener {
            ball6TV.setBackgroundColor(Color.RED)
            score += 0
            ball4TV.isClickable = false
            ball5TV.isClickable = false
            ball6TV.isClickable = false
        }
    }

    private fun easyQuestion5() {
        argentineTV.setOnClickListener {
            argentineTV.setBackgroundColor(Color.GREEN)
            score++
            argentineTV.isClickable = false
            brazilianTV.isClickable = false
            georgianTV.isClickable = false
        }
        brazilianTV.setOnClickListener {
            brazilianTV.setBackgroundColor(Color.RED)
            score += 0
            argentineTV.isClickable = false
            brazilianTV.isClickable = false
            georgianTV.isClickable = false
        }
        georgianTV.setOnClickListener {
            georgianTV.setBackgroundColor(Color.RED)
            score += 0
            argentineTV.isClickable = false
            brazilianTV.isClickable = false
            georgianTV.isClickable = false
        }
    }

    fun submitButton(view: View) {
        if (!messiTV.isClickable || !ronaldoTV.isClickable || !ronaldinhoTV.isClickable) {
            if (!realMadridTV.isClickable || !milanTV.isClickable || !liverpoolTV.isClickable) {
                if (!cruyffTV.isClickable || !guardiolaTV.isClickable || !zidaneTV.isClickable) {
                    if (!ball4TV.isClickable || !ball5TV.isClickable || !ball6TV.isClickable) {
                        if (!argentineTV.isClickable || !georgianTV.isClickable || !brazilianTV.isClickable) {
                            startActivity(Intent(this, EasyQuestionsAnswer::class.java))
                        } else {
                            Toast.makeText(this, "please fix the questions!", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this, "please fix the questions!", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "please fix the questions!", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "please fix the questions!", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "please fix the questions!", Toast.LENGTH_SHORT).show()
        }
    }
}