აპლიკაციაში წარმოდგენილია ორი "სექცია". ერთში წარმოდგენილია ფეხბურთის კლუბები და თითოუელს თავისი
მცირე ისტორია უწერია,ხოლო მეორეში ფეხბურთელები და თითოეულის შესახებ მცირე მნიშვნელოვანი ინფორმცია
წერია. ამ ინფორმაციაზე დაყრდნობით მომხარებელს შეუძლია გააკეთოს ქვიზი, აირჩოს სასურველი დონე: მარტივი,
საშუალო და რთული და შესაბამისად გასცეს კითხვებზე პასუხი, შემდეგ კი გაიგოს შედეგი. აპლიკაცია ეხმარება
მომხარებელს რომ ფეხბურთის გამერჩეულ კლუბებსა თუ ფეხბრუთელებზე გაიგოს მნიშვნელოვანი ინფორმაცია.